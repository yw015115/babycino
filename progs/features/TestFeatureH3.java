class TestFeatureH {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

//The do statement executes first, 'done' remains false and the loop continues
//multiple times until 'done' becomes true. Remove the '!' operator and the
//loop iterates once because 'done' evaluates to false.
class Test {

    public int f() {
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
  done = false;
	do {
	    result = result + count;
	    count = count + 1;
	    done = (10 < count);
	} while (!done);
	return result;
    }

}
