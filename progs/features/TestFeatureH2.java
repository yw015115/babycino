class TestFeatureH {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

//If the returned result is greater than 0 then the do statement must have been
//executed at least once, which would not happen if 'done' was being evaluated
//at the start of the loop & result in 0.
class Test {

    public int f() {
	int result;
	int count;
	boolean done;
	result = 0;
	count = 2;
  done = false;
	do {
	    result = result + count;
	    count = count + 1;
	    done = (10 < count);
	} while (done);
	return result;
    }

}
